PKG_NAME       = packt-sync
BIN            = packt-sync
OUTPUT_DIR     = builds
TMP_DIR        := .tmp
RELEASE_VER    = 1.0.1-$(shell git rev-parse --short HEAD)

PWD := $(shell pwd)

.PHONY: help all docker
.DEFAULT_GOAL := help

all: test build ## Test, build

test: ## Perform both unit and integration tests
	go test -v $(shell go list ./... | grep -v /vendor/) -cover

build: clean ## Build for linux, darwin and windows (save to builds)
	gox -ldflags "-X main.version=$(RELEASE_VER)" -osarch "linux/amd64 darwin/amd64 windows/amd64" -output "$(OUTPUT_DIR)/{{.OS}}/{{.Arch}}/${BIN}" -parallel 3

build/windows: clean/windows ## Build for linux (save to builds/windows)
	gox -ldflags "-X main.version=$(RELEASE_VER)" -osarch "linux/amd64" -output "$(OUTPUT_DIR)/{{.OS}}/{{.Arch}}/${BIN}" -parallel 1

build/linux: clean/linux ## Build for linux (save to builds/linux)
	gox -ldflags "-X main.version=$(RELEASE_VER)" -osarch "linux/amd64" -output "$(OUTPUT_DIR)/{{.OS}}/{{.Arch}}/${BIN}" -parallel 1

build/darwin: clean/darwin ## Build for darwin (save to builds/darwin)
	gox -ldflags "-X main.version=$(RELEASE_VER)" -osarch "darwin/amd64" -output "$(OUTPUT_DIR)/{{.OS}}/{{.Arch}}/${BIN}" -parallel 1

install: 	build/$(shell go env GOHOSTOS)
	cp $(OUTPUT_DIR)/$(shell go env GOHOSTOS)/$(shell go env GOHOSTARCH)/$(PKG_NAME)* $(shell go env GOPATH)/bin/

clean: clean/darwin clean/linux clean/windows ## Remove all build artifacts

clean/darwin: ## Remove darwin build artifacts
	$(RM) -rf $(OUTPUT_DIR)/darwin

clean/linux: ## Remove linux build artifacts
	$(RM) -rf $(OUTPUT_DIR)/linux

clean/windows:
	$(RM) -rf $(OUTPUT_DIR)/windows


help: ## Display this help message
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_\/-]+:.*?## / {printf "\033[34m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | \
		sort | \
		grep -v '#'
