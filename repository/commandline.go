package repository

import (
	"fmt"
	"os"

	"github.com/Sirupsen/logrus"

	"gitlab.com/apetitbois/packt-sync/utils"
	cli "gopkg.in/urfave/cli.v2"
)

// Initialize is the entry point for cli.v2 as the init command
var Initialize = &cli.Command{
	Name:      "init",
	Usage:     "initializes eBook repository",
	ArgsUsage: "",
	Action:    initRepository,
}

// Process is the entry point for cli.v2 of the run command
var Process = &cli.Command{
	Name:      "run",
	Usage:     "run synchronization of eBook repository",
	ArgsUsage: "",
	Action:    process,
}

func setLogLevel(c *cli.Context) {
	switch c.String("log-level") {
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
		return
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
		return
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
		return
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
		return
	}
}

func initRepository(c *cli.Context) error {
	setLogLevel(c)
	configFile := c.String("config")

	if c.Args().Len() > 0 {
		cli.ShowCommandHelp(c, "init")
		return fmt.Errorf("[ERROR] Too many arguments given to init")
	}
	logrus.Infof("Initializing repository")
	uname, _ := utils.ReadString("Username")
	password, _ := utils.ReadString("Password (leave empty to prompt everytime)", "")
	cwd, _ := os.Getwd()
	directory, _ := utils.ReadString("eBook collection directory", cwd+"/eBooks")

	rep := Repository{
		Username:  uname,
		Password:  password,
		Directory: directory,
	}
	rep.SaveToFile(configFile)
	rep.CreateBookDirectory()

	return nil
}

func process(c *cli.Context) error {
	setLogLevel(c)
	logrus.Infof("Synchronizing eBook Library")
	defer logrus.Infof("Completed synchronization of Library")
	configFile := c.String("config")

	R, err := LoadFromFile(configFile)
	if err != nil {
		return fmt.Errorf("[ERROR] Unable to load configuration file (%v)", err)
	}

	R.Authenticate()
	if err := R.fetchBookList(); err != nil {
		return err
	}
	R.DownloadBooks(c.String("format"))
	return nil
}
