package repository

import (
	"fmt"
	"io"

	"github.com/Sirupsen/logrus"
	"golang.org/x/net/html"
)

func (repo *Repository) fetchBookList() error {
	resp, err := repo.client.Get("https://www.packtpub.com/account/my-ebooks")
	if err != nil {
		return fmt.Errorf("Unable to obtain booklist: %v", err)
	}
	list := processBody(resp.Body)
	if len(list) == 0 {
		return fmt.Errorf("No books were found")
	}
	repo.bookCollection = list
	logrus.Infof("Found %v books", len(list))
	return nil
}

func processBody(resp io.Reader) map[string]string {
	var title, nid string
	ret := make(map[string]string)
	z := html.NewTokenizer(resp)
	i := 0
	for {
		i++
		tt := z.Next()
		if tt == html.ErrorToken {
			return ret
		}
		n, hasAttr := z.TagName()
		title, nid = "", ""
		if hasAttr && string(n) == "div" {
			moreAttr := true
			for moreAttr == true {
				var k, v []byte
				k, v, moreAttr = z.TagAttr()
				key, value := string(k), string(v)
				if key == "class" && (value == "product-line seen" || value == "product-line unseen") {
					for moreAttr {
						k, v, moreAttr = z.TagAttr()
						key, value := string(k), string(v)
						if key == "nid" {
							nid = value
						}
						if key == "title" {
							title = value
						}
					}
					logrus.Debugf("%v (%v)", title, nid)
					ret[nid] = title
				}
			}
		}
	}
}
