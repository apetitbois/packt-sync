package repository

import (
	"io"
	"net/url"
	"strings"

	"github.com/Sirupsen/logrus"
	"golang.org/x/net/html"
)

// Authenticate will authenticate against packtpub and fetch cookies
func (repo *Repository) Authenticate() error {
	resp, err := repo.client.Get("https://www.packtpub.com/")
	if err != nil {
		return err
	}
	token := getFormTokenFromBody(resp.Body)
	logrus.Debugf("Using token %v to authenticate", token)

	v := url.Values{
		"email":         {repo.Username},
		"password":      {repo.Password},
		"op":            {"Login"},
		"form_build_id": {token},
		"form_id":       {"packt_user_login_form"},
	}
	logrus.Debugf("Parameters : %#v", v)
	resp, err = repo.client.PostForm("https://www.packtpub.com", v)
	if err != nil {
		return err
	}
	return nil
}

func getFormTokenFromBody(resp io.Reader) string {
	var found bool
	z := html.NewTokenizer(resp)
	i := 0
	for {
		i++
		tt := z.Next()
		if tt == html.ErrorToken {
			return ""
		}
		n, hasAttr := z.TagName()
		if hasAttr {
			moreAttr := true
			for moreAttr == true {
				var k, v []byte
				k, v, moreAttr = z.TagAttr()
				name, key, value := string(n), string(k), string(v)
				if name == "div" && key == "id" && value == "login-form" {
					found = true
				}
				// We are looking for the first form- token after having found the login-form tag
				if found && (name == "input") && (key == "value") && (strings.HasPrefix(value, "form-")) {
					return value
				}
			}
		}
	}
}
