package repository

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/cookiejar"
	"os"

	"github.com/Sirupsen/logrus"
)

// Repository represents the configuration of the current repository
type Repository struct {
	Username       string `json:"username"`
	Password       string `json:"password"`
	Directory      string `json:"content_directory"`
	Format         string `json:"format"`
	token          string
	client         *http.Client
	bookCollection map[string]string
}

// LoadFromFile will create a Repository object from filename
func LoadFromFile(filename string) (*Repository, error) {
	r := Repository{}
	err := r.LoadFromConfig(filename)
	return &r, err
}

// LoadFromConfig setsup the repository based on the configuration file passed as filename
func (repo *Repository) LoadFromConfig(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	fstat, _ := f.Stat()
	fsize := fstat.Size()
	buffer := make([]byte, fsize)
	_, err = f.Read(buffer)
	if err != nil {
		return err
	}
	err = json.Unmarshal(buffer, repo)
	if err != nil {
		return err
	}
	repo.client = http.DefaultClient
	repo.client.Jar, _ = cookiejar.New(&cookiejar.Options{})
	return nil
}

// SaveToFile will serialize the configuration to disk
func (repo *Repository) SaveToFile(filename string) error {
	configuration, err := json.MarshalIndent(repo, "", "\t")
	if err == nil {
		f, oferr := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0755)
		if oferr != nil {
			return fmt.Errorf("[ERROR] Unable to open file configuration.json: %v", oferr)
		}
		// Truncates the content of the file in case the new configuration is shorter than the previous one
		f.Truncate(0)
		defer f.Close()
		_, err = f.Write(configuration)
		if err != nil {
			return fmt.Errorf("[ERROR] Unable to save configuration: %v", err)
		}
		logrus.Infof("Configuration saved as %v", filename)
		return nil
	}
	return fmt.Errorf("[ERROR] Unable to marshal the configuration, %v", err.Error())
}

// CreateBookDirectory will create the directory structure to store the book collection
func (repo *Repository) CreateBookDirectory() error {
	err := os.Mkdir(repo.Directory, 0775)
	if err != nil {
		if os.IsExist(err) {
			logrus.Warningf("Directory %v already existed", repo.Directory)
			return nil
		}
		return fmt.Errorf("[ERROR] %v", err)
	}
	logrus.Infof("Directory %v created", repo.Directory)
	return nil
}

// DownloadBooks will process the library and download books in the specified "format"
func (repo *Repository) DownloadBooks(format string) {
	f := []string{}
	if repo.Format != "" {
		format = repo.Format
	}
	if format == "" {
		logrus.Warning("No format selected, defaulting to epub")
		format = "epub"
	}
	if format == "all" {
		f = []string{"epub", "mobi", "pdf"}
	} else {
		f = append(f, format)
	}
	logrus.Infof("Downloading books in the following format(s): %v", f)
	for nid, title := range repo.bookCollection {
		logrus.Debugf("Downloading book \"%v\" at URL https://download.packepub.com/ebook/%v/%v", title, format, nid)
		for _, ftype := range f {
			url := fmt.Sprintf("https://www.packtpub.com/ebook_download/%s/%s", nid, ftype)
			destination := fmt.Sprintf("%s/%s.%s", repo.Directory, title, ftype)
			if err := repo.downloadBook(url, destination); err != nil {
				logrus.Errorf("Unable to download book \"%s\" in format %s: %v", title, ftype, err)
			}
		}
	}
}

func (repo *Repository) downloadBook(url, destination string) error {
	_, err := os.Stat(destination)
	if err != nil {
		out, err := os.Create(destination)
		if err != nil {
			return fmt.Errorf("Create: %v", err)
		}
		defer out.Close()
		resp, err := repo.client.Get(url)
		if err != nil {
			return fmt.Errorf("Get: %v", err)
		}
		defer resp.Body.Close()
		n, err := io.Copy(out, resp.Body)
		if err != nil {
			return fmt.Errorf("%v", err)
		}
		if n == 0 {
			out.Close()
			os.Remove(destination)
			return fmt.Errorf("Book not found")
		}
	} else {
		logrus.Debugf("Skipping \"%v\", already present", destination)
	}
	return nil
}
