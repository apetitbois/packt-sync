# Packt-sync
[![build status](https://gitlab.com/apetitbois/packt-sync/badges/master/build.svg)](https://gitlab.com/apetitbois/packt-sync/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/apetitbois/packt-sync)](https://goreportcard.com/report/gitlab.com/apetitbois/packt-sync)

Packt-sync is an open source project that helps users maintain in sync their Packt.com ebook collection with a local repository of books.

## Installation

## Usage
```
NAME:
   packt-sync - packt-sync is a synchonization service for packtpub.com ebooks

USAGE:
   packt-sync [global options] command [command options] [arguments...]

VERSION:
   1.0.1

COMMANDS:
     init     initializes eBook repository
     run      run synchronization of eBook repository
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --config value, -c value, --configuration value  Speficy configuration file to use (default: "configuration.json")
   --log-level value, -l value                      Output level (debug, info, warn, error) (default: "info")
   --format value, -f value                         Format to download eBooks as (pdf, epub, mobi, all) (default: "")
   --help, -h                                       show help (default: false)
   --version, -v                                    print the version (default: false)
```

## Example configuration
```
{
	"username": "<user@host.net>",
	"password": "********",
	"content_directory": "/Users/john/Documents/eBooks/PDF",
	"format": "pdf"
}
```
