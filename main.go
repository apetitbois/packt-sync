package main

import (
	"os"

	"gitlab.com/apetitbois/packt-sync/repository"

	"gopkg.in/urfave/cli.v2"
)

var (
	version, verbose, format string
	configFile               string
)

func main() {
	app := &cli.App{
		Name:    "packt-sync",
		Usage:   "packt-sync is a synchonization service for packtpub.com ebooks",
		Version: version,
		Action:  func(c *cli.Context) error { return nil },
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "config",
				Aliases:     []string{"c", "configuration"},
				Value:       "configuration.json",
				Usage:       "Speficy configuration file to use",
				Destination: &configFile,
			},
			&cli.StringFlag{
				Name:        "log-level",
				Aliases:     []string{"l"},
				Value:       "info",
				Usage:       "Output level (debug, info, warn, error)",
				Destination: &verbose,
			},
			&cli.StringFlag{
				Name:        "format",
				Aliases:     []string{"f"},
				Value:       "",
				Usage:       "Format to download eBooks as (pdf, epub, mobi, all)",
				Destination: &format,
			},
		},
		Commands: []*cli.Command{
			repository.Initialize,
			repository.Process,
		},
	}

	if len(os.Args) <= 1 {
		app.Run(append(os.Args, "help"))
	} else {
		app.Run(os.Args)
	}
}
