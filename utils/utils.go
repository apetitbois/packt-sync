package utils

import (
	"bufio"
	"fmt"
	"os"
)

// ReadString is reading input from os.Stdin and makes it a string
// if def is not empty, this will allow for an empty string to be
// specified and def will be set in that case
func ReadString(inputMessage string, defString ...string) (string, error) {
	defaultValue := ""
	if len(defString) > 0 {
		defaultValue = defString[0]
	}
	for {
		r := bufio.NewReader(os.Stdin)
		if len(defString) > 0 {
			fmt.Printf("%s [%s]: ", inputMessage, defaultValue)
		} else {
			fmt.Printf("%s: ", inputMessage)

		}
		res, err := r.ReadString('\n')
		if err != nil {
			return "", err
		}
		if len(res) >= 0 {
			res = res[:len(res)-1]
		}
		if len(defString) > 0 {
			if res == "" {
				return defaultValue, nil
			}
			return res, nil
		}
		if res != "" {
			return res, nil
		}

	}
}
